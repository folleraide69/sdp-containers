#include <gtest/gtest.h>

#include <exception>

#include "../src/HeterogenList.hpp"

using eu::petartoshev::fmi::sdp::containers::HeterogenList;

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {
namespace test {

#define INPUT_FILE "test/data/t1.in"
#define OUTPUT_FILE "build/out/t1.out"

TEST(HeterogenListTests, Initialize) {
  HeterogenList list;
  ASSERT_FALSE(false);
}

TEST(HeterogenList, ReadAndSave) {
  HeterogenList l;
  l.Read(INPUT_FILE);
  l.Save(OUTPUT_FILE);

  std::string l1;
  std::string l2;
  std::ifstream in(INPUT_FILE);
  std::ifstream out(OUTPUT_FILE);
  if (!in.is_open()) FAIL() << "in cannot be opened";
  if (!out.is_open()) FAIL() << "out cannot be opened";
  bool f1;
  bool f2;

  int line = 0;
  while (getline(in, l1) && getline(out, l2) || getline(out, l2)) {
    if (in.eof()) FAIL() << "in has ended unexpectedly";
    if (out.eof()) FAIL() << "out has ended unexpectedly";
    line++;
    ASSERT_EQ(l1, l2) << "Error on line " << line << "\nExpected: " << l1
                      << "\nGot     : " << l2;
  }
  ASSERT_TRUE(in.eof());
  ASSERT_TRUE(out.eof());
}

}  // namespace test
}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu
