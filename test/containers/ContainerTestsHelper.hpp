#ifndef TEST_PARENT
#error TEST_PARENT_NOT_DEFINED
#endif

TYPED_TEST(TEST_PARENT, GetSize__Should_ReturnSize0_When_Empty) {
  ASSERT_EQ(this->container.GetSize(), 0);
}

TYPED_TEST(TEST_PARENT, IsEmpty__Should_BeEmpty_When_Empty) {
  ASSERT_TRUE(this->container.IsEmpty());
}

TYPED_TEST(TEST_PARENT, Remove__Should_ThrowException_When_Empty) {
  ASSERT_THROW(this->container.Remove(), std::out_of_range);
}

TYPED_TEST(TEST_PARENT, Sort__Should_SortElements) {
  for (unum i = 0; i < MAGIC_LEN; i++) this->container.Add(this->x[i]);

  this->container.Sort();
  std::sort(this->x, this->x + MAGIC_LEN);

  for (unum i = 0; i < MAGIC_LEN; i++)
    ASSERT_DOUBLE_EQ(this->x[i], this->container.Remove());
}

#undef TEST_PARENT