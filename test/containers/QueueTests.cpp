#include <gtest/gtest.h>

#include "../../src/containers/Queue.hpp"
#include "./ContainerTests.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {
namespace test {

template <typename T>
class QueueTests
    : public eu::petartoshev::fmi::sdp::containers::test::ContainerTests<T> {
 public:
  QueueTests()
      : ContainerTests<T>(
            *(new eu::petartoshev::fmi::sdp::containers::Queue<T>)) {}
  // eu::petartoshev::fmi::sdp::containers::Queue<T> queue;
};

TYPED_TEST_SUITE(QueueTests, NumericImplementations);

TYPED_TEST(QueueTests, Add_Remove__Should_AddElementsToQueue) {
  for (unum i = 0; i < MAGIC_LEN; i++)
    ASSERT_TRUE(this->container.Add(this->x[i]));
  ASSERT_EQ(this->container.GetSize(), MAGIC_LEN);

  for (unum i = 0; i < MAGIC_LEN; i++)
    ASSERT_DOUBLE_EQ(this->x[i], this->container.Remove());
  ASSERT_EQ(this->container.GetSize(), 0);
}

#define TEST_PARENT QueueTests
#include "./ContainerTestsHelper.hpp"

}  // namespace test
}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu