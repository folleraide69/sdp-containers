#ifndef SDP_CONTAINERS_SRC_NODE_HPP
#define SDP_CONTAINERS_SRC_NODE_HPP

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

/**
 * Node for linked list
 */
template <typename T>
class Node {
 public:
  Node<T>(Node<T>* previous, T& data, Node<T>* next = nullptr) {
    this->previous = previous;
    this->data = data;
    this->next = next;
  }
  Node<T>* GetPrev() { return this->previous; }
  Node<T>* GetPrev() const { return this->previous; }
  T& Get() { return this->data; }
  T& Get() const { return this->data; }
  Node<T>* GetNext() { return this->next; }
  Node<T>* GetNext() const { return this->next; }
  void SetPrev(Node* previous) { this->previous = previous; }
  void set(T data) { this->data = data; }
  void SetNext(Node* next) { this->next = next; }
  static Node<T>* GetPrevOnNode(Node<T>* node) { return node->previous; }
  static Node<T>* GetNextOnNode(Node<T>* node) { return node->next; }

 private:
  Node<T>* previous;
  T data;
  Node<T>* next;
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINERS_SRC_NODE_HPP