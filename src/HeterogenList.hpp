#ifndef SDP_CONTAINER_SRC_HETEROGEN_LIST_HPP
#define SDP_CONTAINER_SRC_HETEROGEN_LIST_HPP

#include <fstream>
#include <functional>
#include <stdexcept>

#include "GenericTypes.hpp"
#include "LinkedList.hpp"
#include "Node.hpp"
#include "containers/Container.hpp"
#include "containers/DoubleLinkedList.hpp"
#include "containers/Queue.hpp"
#include "containers/Stack.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

#define DEFAULT_TYPE int
#define CONTAINER_DEFAULT Container<DEFAULT_TYPE>*
#define CONTAINER_GENERIC Container<void*>*

class HeterogenList {
 public:
  /// import data from file to current list
  bool Read(const char* filename);
  /// Load balanced add
  bool Add(DEFAULT_TYPE& element) {
    CONTAINER_DEFAULT min = (CONTAINER_DEFAULT)((void*)this->list.Begin());
    for (auto it = this->list.Begin(); it != this->list.End(); it++)
      if (((CONTAINER_DEFAULT)((void*)it))->GetSize() < min->GetSize())
        min = ((CONTAINER_DEFAULT)((void*)it));
    return min->Add(element);
  }
  bool AddToContainer(const unum containerIndex, DEFAULT_TYPE& element) {
    CheckIndex(containerIndex);
    return ((CONTAINER_DEFAULT)this->list.Get(containerIndex))->Add(element);
  }
  bool Contains(const DEFAULT_TYPE& el) {
    bool found = false;
    for (auto it = this->list.Begin(); it != this->list.End() && !found; it++) {
      found = ((CONTAINER_DEFAULT)((void*)it))->Contains(el);
    }
    return found;
  }
  void Filter(CONDITION_TYPED(DEFAULT_TYPE) predicate) {
    ForEach([&predicate](CONTAINER_DEFAULT elem) { elem->Filter(predicate); });
  }
  void Print(std::ostream& out) const {
    ForEach([&out](CONTAINER_DEFAULT elem) { elem->Print(out); });
  }
  DEFAULT_TYPE RemoveFromContainer(const unum containerIndex) {
    CheckIndex(containerIndex);
    return ((CONTAINER_DEFAULT)this->list.Get(containerIndex))->Remove();
  }
  bool Save(const char* filename) {
    std::ofstream out(filename);
    if (!out.is_open()) {
      std::cerr << "Cannot open file " << filename;
      return false;
    }
    this->Print(out);
    out.close();
    return true;
  }
  void SortContainers() {
    ForEach([](CONTAINER_DEFAULT elem) { elem->Sort(); });
  }

  // TODO: Petar Toshev 18.11.2019 Create templated function for deleting
  // Container and remove Template from Heterogen List

 private:
  void CheckIndex(const unum& containerIndex) {
    if (this->list.GetSize() <= containerIndex || containerIndex < 0)
      throw std::invalid_argument("Wrong container index: " + containerIndex);
  }
  CONTAINER_GENERIC CreateNewContainer(const char& containerType) {
    switch (containerType) {
      case 'D':
        return (CONTAINER_GENERIC) new DoubleLinkedList<DEFAULT_TYPE>;
      case 'S':
        return (CONTAINER_GENERIC) new Stack<DEFAULT_TYPE>;
      case 'Q':
        return (CONTAINER_GENERIC) new Queue<DEFAULT_TYPE>;

      default:
        throw std::invalid_argument("Wrong container type: " + containerType);
    }
  }
  void ForEach(std::function<void(CONTAINER_DEFAULT)> manipulate) const {
    for (auto it = this->list.Begin(); it != this->list.End(); it++) {
      manipulate((CONTAINER_DEFAULT)((void*)it));
    }
  }
  void ForEach(std::function<void(CONTAINER_DEFAULT)> manipulate) {
    for (auto it = this->list.Begin(); it != this->list.End(); it++) {
      manipulate((CONTAINER_DEFAULT)((void*)it));
    }
  }

  LinkedList<CONTAINER_GENERIC> list;
  // eu::petartoshev::fmi::sdp::containers::LinkedList<void*> listyyyyy(false);
};

bool HeterogenList::Read(const char* filename) {
  std::cout << "Filename: " << filename << std::endl;
  std::ifstream dataStream(filename);
  // Open the File
  if (dataStream.is_open()) {
    char peek;
    char containerType;
    int i;
    while (dataStream.peek() != EOF) {
      // read container type
      dataStream >> containerType;
      Container<void*>* container = CreateNewContainer(containerType);
      this->list.Add(container);

      // read container data
      while ((peek = dataStream.peek()) != '\n' && peek != EOF) {
        dataStream >> i;
        ((Container<int>*)container)->Add(i);
      }
      dataStream.ignore();
    }
    dataStream.close();
    return true;
  }
  return false;
}

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_HETEROGEN_LIST_HPP