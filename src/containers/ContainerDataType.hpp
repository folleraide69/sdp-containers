#ifndef SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_DATA_TYPE_HPP
#define SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_DATA_TYPE_HPP

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

enum ContainerDataType {
  INTEGER,
  SHORT,
  LONG,
  LONG_LONG,
  UNSIGNED_INT,
  UNSIGNED_SHORT,
  UNSIGNED_LONG,
  UNSIGNED_LONG_LONG
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_DATA_TYPE_HPP