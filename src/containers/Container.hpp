#ifndef SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_HPP
#define SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_HPP

#include "../LinkedList.hpp"
#include "ContainerIterator.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

/**
 * Abstract template class for all containers
 */
template <typename T>
class Container {
 public:
  virtual bool Add(T& element) { return this->list.Add(element); }
  virtual bool Contains(const T& element) const {
    return this->list.Contains(element);
  };
  virtual bool Contains(CONDITION predicate) const {
    return this->list.Contains(predicate);
  }
  virtual void Clear() { this->list.Clear(); };
  virtual unum GetSize() const { return this->list.GetSize(); }
  virtual bool IsEmpty() const { return this->list.IsEmpty(); }
  virtual void Filter(CONDITION predicate) { this->list.Filter(predicate); }
  virtual void Print(std::ostream& out) const {
    out << type << ' ';
    this->list.Print(out);
    out << std::endl;
  }
  virtual void Sort() { this->list.Sort(); }
  virtual T Remove() = 0;
  // ContainerIterator<T> begin() const { return ContainerIterator<T>(front);
  // } ContainerIterator<T> end() const { return I(); }

 protected:
  Container(const char type) : type(type) {}
  LinkedList<T> list;
  const char type;
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_HPP