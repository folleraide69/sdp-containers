#ifndef SDP_CONTAINER_SRC_CONTAINERS_STACK_HPP
#define SDP_CONTAINER_SRC_CONTAINERS_STACK_HPP

#include "./Container.hpp"

using eu::petartoshev::fmi::sdp::containers::Container;

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class Stack : public Container<T> {
 public:
  Stack() : Container<T>('S') {}
  virtual void Sort() {
    Container<T>::Sort();
    this->list.Reverse();
  }
  virtual T Remove() { return this->list.Remove(this->GetSize() - 1); }
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_CONTAINERS_STACK_HPP