#ifndef SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_ITERATOR_HPP
#define SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_ITERATOR_HPP

#include "../Node.hpp"
#include "Container.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class Container;

/**
 * Iterator for containers
 */
template <typename T>
class ContainerIterator {
 private:
  friend class Container<T>;
  Node<T>* node;

 public:
  ContainerIterator(Node<T>* ptr = nullptr) : node(ptr) {}
  T const& GetConst() const { return node->Get(); }
  T& Get() const { return node->Get(); }
  bool HasNext() { return node->GetNext() != nullptr; }
  ContainerIterator<T> Next() const {
    return ContainerIterator(node->GetNext());
  }
  bool Valid() const { return node != nullptr; }
  operator bool() const { return Valid(); }
  operator T() const { return node->data; }
  operator void*() const { return (void*)node->data; }
  ContainerIterator<T>& operator++() { return (*this = Next()); }
  ContainerIterator<T> operator++(int) {
    ContainerIterator<T> save = *this;
    ++(*this);
    return save;
  }
  bool operator==(ContainerIterator<T> const& it) const {
    return node == it.node;
  }
  bool operator!=(ContainerIterator<T> const& it) const {
    return !(*this == it);
  }

  ContainerIterator<T>& operator+=(unsigned int n) {
    for (unsigned int i = 0; i < n; i++) ++(*this);
    return *this;
  }
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_ITERATOR_HPP