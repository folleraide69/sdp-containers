#ifndef SDP_CONTAINER_SRC_CONTAINERS_DOUBLE_LINKED_LIST_HPP
#define SDP_CONTAINER_SRC_CONTAINERS_DOUBLE_LINKED_LIST_HPP

#include "./Container.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class DoubleLinkedList : public Container<T> {
 public:
  DoubleLinkedList() : Container<T>('D') {}
  virtual T Remove() { return this->list.Remove(this->list.GetSize() - 1); }
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_CONTAINERS_DOUBLE_LINKED_LIST_HPP