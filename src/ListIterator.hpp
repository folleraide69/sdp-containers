#ifndef SDP_CONTAINER_SRC_LIST_ITERATOR_HPP
#define SDP_CONTAINER_SRC_LIST_ITERATOR_HPP

#include "LinkedList.hpp"
#include "Node.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class LinkedList;

/**
 * Iterator for templated linked list
 */
template <typename T>
class ListIterator {
 private:
  friend class LinkedList<T>;
  Node<T>* node;

 public:
  ListIterator(Node<T>* ptr = nullptr) : node(ptr) {}
  T const& GetConst() const { return node->Get(); }
  T& Get() const { return node->Get(); }
  bool HasNext() const { return node->GetNext() != nullptr; }
  bool HasPrev() const { return node->GetPrev() != nullptr; }
  ListIterator<T> Next() const { return ListIterator(node->GetNext()); }
  ListIterator<T> Prev() const { return ListIterator(node->GetPrev()); }
  bool Valid() const { return node != nullptr; }
  operator bool() const { return Valid(); }
  operator T() const { return Get(); }
  operator void*() const { return (void*)Get(); }
  ListIterator<T>& operator++() { return (*this = Next()); }
  ListIterator<T> operator++(int) {
    ListIterator<T> save = *this;
    ++(*this);
    return save;
  }
  ListIterator<T>& operator+=(unsigned int n) {
    for (unsigned int i = 0; i < n; i++) ++(*this);
    return *this;
  }
  ListIterator<T>& operator--() { return (*this = Prev()); }
  ListIterator<T> operator--(int) {
    ListIterator<T> save = *this;
    --(*this);
    return save;
  }
  ListIterator<T>& operator-=(unsigned int n) {
    for (unsigned int i = 0; i < n; i++) --(*this);
    return *this;
  }
  bool operator==(ListIterator<T> const& it) const { return node == it.node; }
  bool operator!=(ListIterator<T> const& it) const { return !(*this == it); }
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_LIST_ITERATOR_HPP